#include <iostream>
#include <conio.h>

using namespace std;

class Str {
	public:
	char *s;
	Str operator+ (Str a) {
		Str res;
		int n = strlen(s) + strlen(a.s) + 1;
		res.s = new char[n];
		strcpy(res.s, s);
		strcat(res.s, a.s);
		return res;
	}
};

int main() {
	int i;
	Str a, b;
	cout << "Hi!" << endl;
	a.s = "str1";
	b.s = "str2";
	Str c = a + b;
	cout << c.s;
	getch();
	return 0;
}
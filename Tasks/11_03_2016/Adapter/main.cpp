// Purpose.  Adapter design pattern lab
// 
// ������.  ����� ������������ "stack machine", ��� �� ��-����������
// ������ Stack.  �� ������ �� �������� ������������ ���� "legacy"
// ����� ��� ������ ��� ������ ������ Queue, �� ������� "�������������� ����������" 
// ����� ������ � ����� �����������.
// 
// �������.
// o ����� Queue ������ ��������� "�������" ���������� Stack.
// o ������ Queue ��������� ������ ctor, dtor(��), enque, deque, � isEmpty.  ������ ��
//   ���� ������� ��������� ��������� �� ���������� Queue � ����������
//   Stack.
// o ���� �� ���������� Queue::enque() �� Stack::push(), �� ����������� ���� ���������
//   ���� ��� ��������� ���������� ������ Queue::deque().

#include <iostream>

using namespace std;

struct StackStruct {
   int*  arr;
   int   sp;
   int   size;
};
typedef StackStruct Stack;


static void initialize( Stack* s, int size ) {
   s->arr = new int[size];
   s->size = size;
   s->sp = 0; }

static void cleanUp( Stack* s ) {
   delete s->arr; }

static int isEmpty( Stack* s ) {
   return s->sp == 0 ? 1 : 0; }

static int isFull( Stack* s ) {
   return s->sp == s->size ? 1 : 0; }

static void push( Stack* s, int item ) {
   if ( ! isFull(s)) s->arr[s->sp++] = item; }

static int pop( Stack* s ) {
   if (isEmpty(s)) return 0;
   else            return s->arr[--s->sp]; }

class Queue {
	Stack s1, s2;
	public:
	Queue(int n) { //�����������
		initialize(&s1, n);
		initialize(&s2, n);
	}

	~Queue() {
		delete s1.arr;
		delete s2.arr;
	}

	void enque(int a) {
		push(&s1, a);
	}

	int deque() {
		int res;
		int i;

		while (!isEmpty(&s1))
			push(&s2, pop(&s1));
		res = pop(&s2);
		while (!isEmpty(&s2))
			push(&s1, pop(&s2));
		return res;
	} 

	int is_Empty () {
		return isEmpty(&s1);
	}
  /* deque pseudocode:
      initialize a local temporary instance of Stack
      loop
         pop() the permanent stack and push() the temporary stack
      pop() the temporary stack and remember this value
      loop
         pop() the temporary stack and push() the permanent stack
      cleanUp the temporary stack
      return the remembered value
   int isFull()  { return ::isFull( &_stack );  }  */
};

int main() {
   Queue  queue(15);
	Stack ss;
	
   for (int i=0; i < 25; i++) queue.enque( i );
   while ( ! queue.is_Empty())
	  cout << queue.deque() << " ";
   cout << endl;


   return 0;
}

// 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 

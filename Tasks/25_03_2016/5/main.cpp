#include <iostream>
#include <fstream>
#include <set>
#include <deque>
#include <string>

using namespace std;

int main(int argc, char* argv[]) {
    ifstream fin;
    deque <string> words;
    set <string> S;
    set <string>::iterator it;
    int i, j;
    string s;

    fin.open(argv[1]);
    while(!(fin.eof())) {
        fin >> s;
        words.push_back(s);
    }

    for(i = 0; i < words.size(); i++) {
        for(j = 0; j < words[i].length(); j++)
            if ((65 <= words[i][j]) && (words[i][j] <= 90))
                words[i][j] = words[i][j] + 32;
    }

    for(i = 0; i < words.size(); i++)
        S.insert(words[i]);

    for(it = S.begin(); it != S.end(); it++)
        cout << *it << endl;

    fin.close();
    return 0;
}

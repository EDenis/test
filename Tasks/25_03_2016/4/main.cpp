#include <iostream>
#include <set>
#include <vector>
#include <string.h>

using namespace std;

int main() {
    set <char> letters;
    char *word = "programming";
    vector <bool> W;
    int i, n, t = 8;
    char c;


    n = strlen(word);
    for(i = 0; i < n; i++) {
        letters.insert(word[i]);
        W.push_back(false);
    }

    while(letters.size() != 0) {
        if (t == 0) {
            cout << "You lost..." << endl;
            return 0;
        }

        cout << "The word is:" << endl;
        for(i = 0; i < n; i++) {
            if (W[i])
                cout << word[i];
            else
                cout << "*";
            }
        cout << endl;
        cout << "You have " << t << " lives" << endl;
        cout << "Enter some letter:" << endl;
        cin >> c;
        if (letters.erase(c) != 0) {
            cout << "You are right!" << endl;
            for(i = 0; i < n; i++)
                if (word[i] == c)
                    W[i] = true;
        } else {
            t--;
            cout << "You are wrong!" << endl;
        }
    }
    cout << "Congratulations!" << endl;
    return 0;
}

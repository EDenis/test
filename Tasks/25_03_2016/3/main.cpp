#include <iostream>
#include <map>
#include <fstream>
#include <string>

using namespace std;
//int argc, char* argv[]
int main(int argc, char* argv[]) {
    ifstream fin;
    char c;
    map <string, int> words;
    map <string, int>::iterator it;
    int i;
    string s;
    fin.open(argv[1]);

    while(!(fin.eof())) {
        fin >> s;
        it = words.find(s);
        if (it == words.end())
            words.insert(pair <string, int>(s, 1));
        else
            it->second++;
    }
    it->second--;
    for(it = words.begin(); it != words.end(); it++) {
      cout << it->first << " : " << it->second << endl;///вывод на экран
    }

    fin.close();
    return 0;
}

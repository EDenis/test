#include <set>
#include <iostream>

using namespace std;

int main() {
    int i, n, a;
    set <int> numbers;
    cout << "How many numbers will you enter?" << endl;
    cin >> n;
    for(i = 0; i < n; i++) {
        cin >> a;
        numbers.insert(a);
    }
    cout << "The number of different elements is " << numbers.size() << endl;
    return 0;
}

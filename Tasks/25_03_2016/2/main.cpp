#include <iostream>
#include <map>
#include <fstream>

using namespace std;

int main(int argc, char* argv[]) {
    ifstream fin;
    char c;
    map <char, bool> letters;
    map <char, bool>::iterator it;
    int i;

    fin.open(argv[1]);
    for(i = 65; i <= 90; i++) {
        letters.insert(pair <char, bool> ((char)i, true));
    }
    while (!fin.eof()) {
        fin >> c;
        if ((65 <= c) && (c <= 90)) {
            letters[c] = false;
        } else if ((c >= 97) && (c <= 122)) {
            letters[c - 32] = false;
        }
    }

    for (it = letters.begin(); it != letters.end(); it++) {
        if (it->second)
            cout << it->first << endl;
    }

    fin.close();
    return 0;
}

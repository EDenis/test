#include <iostream>

using namespace std;

template <class T>
class SmartPointer {

    unsigned * ref_count;
public:
	 T * obj;
	SmartPointer(T * object){
		obj = object;
		ref_count = new unsigned;
		*ref_count = 1;
        /* �� ����� ���������� �������� T * obj � ���������� �������
         * ������ � 1. */
        /* set T *obj and ref_count = 1
        */
    }

    SmartPointer(SmartPointer<T> & sptr) {
		obj = sptr.obj;
		ref_count = sptr.ref_count;
		*ref_count = (*ref_count) + 1;
        /* ���� ����������� ������� ����� ���������������� ��������� �� ������������
         * ������. ��� ����� ������ ���������� obj � ref_count
         * ������ ��, ��� � sptr. �����,
         * ��������� �� ������� ����� ������ �� obj, ��� �����
         * ��������� ref_count. */
         /*
        Set obj and ref_count as in sptr, increment ref_count
         */
    }

    ~SmartPointer() {
        /* ���������� ������ �� ������. ���������
         * ref_count. ���� ref_count = 0, ����������� ������ �
         * ���������� ������. */
         /*
         decrement ref_count. if ref_count = 0, free memory
         */
		*ref_count = (*ref_count) - 1;
		cout << "Deleting! References left: " << *ref_count << endl;
		if (*ref_count == 0) delete(obj);
    }

    SmartPointer<T> & operator=(SmartPointer<T> & sptr) {
        /* ����������� =. ���������� ������ ������� ���������
         * ����� ���������, � ������ - ���������.
         */
        /*
        decrement ref_count, set obj and ref_count, increment ref_count
        */
		(*ref_count)--;
		if (*ref_count == 0) delete(obj);
		obj = sptr.obj;
		ref_count = sptr.ref_count;
		(*ref_count)++;
		return *this;
    }

    T& operator *() {
        return *obj;
    }

    T* operator->() {
        return obj;
    }
};

class Int {
    public:
    Int(int y):x(y){}
    int x;
};


int main() {
	int *a, *b, *c;
	a = new int(3);
	b = new int(4);
	c = new int(5);
	SmartPointer <int> p1(a), p2(b), p3(c);
	cout << *p1 << *p2 << *p3 << endl;
	p1 = p2;
	*(p2.obj) = 1;
	cout << *p1 << *p2 << *p3<< endl;
	SmartPointer <int> p4(p3);
	cout << *p1 << *p2 << *p3 << *p4 << endl;
    SmartPointer<Int> A(new Int(7));
    A->x = 8;
    cout << A->x << endl;

	return 0;
}
